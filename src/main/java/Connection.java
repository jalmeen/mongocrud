import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;


public class Connection {


    protected MongoClient mongoClient;

    public void makeConnection() {

        mongoClient = new MongoClient(new MongoClientURI("mongodb://192.168.43.146:27018"));

        String connectPoint = mongoClient.getConnectPoint();
        System.out.println(connectPoint);
        System.out.println("Connected to the database successfully");
    }

    public void closeConnection(){
        mongoClient.close();
    }

}
