public class Main {

    public static void main(String[] args) {
        Crud crud = new Crud();
        crud.makeConnection();

        crud.dbs();

        crud.createCollection();
        crud.createDocument();

        crud.findDocumentInserted();
        crud.updateDocument();

        crud.findDocumentInserted();

        crud.deleteDocument();
        crud.findDocumentInserted();
    }
}
